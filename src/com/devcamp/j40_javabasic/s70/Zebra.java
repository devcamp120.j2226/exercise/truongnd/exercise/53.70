package com.devcamp.j40_javabasic.s70;

public class Zebra extends Aminal  implements IRunable,IMammal,IMateable{

 public void animalSound() {
  System.out.println("Neigh!");
  
 }

 @Override
 public void running() {
  System.out.println("Horse running...");
 }
 @Override
 public void isMammal() {
  System.out.println("Horse mammal...");
 }
 @Override
 public void mate() {
  System.out.println("Horse mate...");
 }
}
