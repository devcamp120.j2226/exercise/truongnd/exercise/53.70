package com.devcamp.j40_javabasic.s70;

public class Fish extends Aminal implements ISwimable,IMammal,IMateable {

 public void animalSound() {
  System.out.println(".....");
 }

 @Override
 public void swim() {
  System.out.println("Fish swimming");
 }
 @Override
 public void isMammal() {
  System.out.println("Fish mammal...");
 }
 @Override
 public void mate() {
  System.out.println("Fish mate...");
 }

}
