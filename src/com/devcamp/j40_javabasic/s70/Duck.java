package com.devcamp.j40_javabasic.s70;

public class Duck  extends Aminal implements IWalkable,IMammal,IMateable {
 
 public void animalSound() {
  System.out.println("Quack quack");
  
 }
 @Override
 public void isMammal() {
  System.out.println("Duck mammal...");
 }
 @Override
 public void mate() {
  System.out.println("Duck mate...");
 }
 @Override
 public void walk() {
  System.out.println("Duck walking...");
 }
}

