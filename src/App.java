import com.devcamp.j40_javabasic.s70.Duck;
import com.devcamp.j40_javabasic.s70.Fish;
import com.devcamp.j40_javabasic.s70.Zebra;

public class App {
    public static void main(String[] args) throws Exception {

        Duck duck = new Duck();
        duck.name = "Duck";
        duck.age = 3;
        duck.weight= 12.00f;
       
        Fish fish = new Fish();
        fish.name = "Fish";
        fish.age = 2;
        fish.weight= 2.00f;

        Zebra zebra = new Zebra();
        zebra.name = "Zebra";
        zebra.age = 15;
        zebra.weight= 60.00f;
        
        System.out.println(duck);
        System.out.println(fish);
        System.out.println(zebra);
    }
}
